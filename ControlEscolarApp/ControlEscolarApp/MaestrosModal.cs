﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolarApp;
using System.IO;
using LogicaNegocio.ControlEscolarApp;

namespace ControlEscolarApp
{
    public partial class MaestrosModal : Form
    {
        private Maestros _maestros;
        private EstadosManejador _estadosManejador;
        private MunicipiosManejador _municipiosManejador;
        private MaestrosManejador _maestrosmanejador;
        private bool _isEnablebinding = false;

        public MaestrosModal()
        {
            _maestros = new Maestros();
            _estadosManejador = new EstadosManejador();
            _municipiosManejador = new MunicipiosManejador ();
            _maestrosmanejador =new MaestrosManejador();
        InitializeComponent();
        }
        private void BindingmaestrosReload()
        {
            txt_NumeroC.Text = _maestros.NControl;
            txtnombre.Text = _maestros.Nombre;
            txtapellidop.Text = _maestros.Apellidop;
            txtapellidom.Text = _maestros.Apellidom;
            txtsexo.Text = _maestros.Sexo;
            txtnacimiento.Text = _maestros.FechadeNa;
            txtcorreo.Text = _maestros.Correo;
            cmbestado.Text = _maestros.Esta;
            cmbmunicipio.Text = _maestros.Muni;
            txt_NumeroC.Text = _maestros.Nocuenta;

        }
        OpenFileDialog ofd = new OpenFileDialog();
        private void BuscarEstados()
        {
            cmbestado.DataSource = _estadosManejador.ObtenerLista();
            cmbestado.DisplayMember = "nombre";

        }
        public MaestrosModal(Maestros maestros)
        {
            this._maestros = maestros;
        }

        private void Btn_archivos_Click(object sender, EventArgs e)
        {
            ofd.Filter = "Archivos PDF(.pdf)|*.pdf|Archivos jpg(.jpg)|*.jpg";
           if (ofd.ShowDialog() == DialogResult.OK)
            {
                txt_archivo.Text = ofd.SafeFileName;
            }
        }

        private void OpenFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
           _maestrosmanejador.GuardarMaestro(_maestros);
            string carpeta = Application.StartupPath + @"\" + txt_NumeroC.Text;

            try
            {
                if (Directory.Exists(carpeta))
                {
                    MessageBox.Show("Carpeta Existe");
                }
                else
                {
                    MessageBox.Show("Creando Carpeta");
                    Directory.CreateDirectory(carpeta);
                   
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Error: " + ex.Message);
            }

        }
        private void Bindingmaestros()
        {
            if (_isEnablebinding)
            {
                if (_maestros.NControl == _maestros.NControl)
                {
                    _maestros.NControl = _maestros.NControl;
                }

                txt_NumeroC.Text = _maestros.NControl;
                txtnombre.Text = _maestros.Nombre;
                txtapellidop.Text = _maestros.Apellidop;
                txtapellidom.Text = _maestros.Apellidom;
                txtsexo.Text = _maestros.Sexo;
                txtnacimiento.Text = _maestros.FechadeNa;
                txtcorreo.Text = _maestros.Correo;
                cmbestado.Text = _maestros.Esta;
                cmbmunicipio.Text = _maestros.Muni;
                txt_NumeroC.Text = _maestros.Nocuenta;
            }
        }
 
        private void BuscarMunicipios(string filtro)

        {
            cmbestado.ValueMember = "codigo";
            filtro = cmbestado.SelectedValue.ToString();
            cmbmunicipio.DataSource = _municipiosManejador.ObtenerLista(filtro);
            cmbmunicipio.DisplayMember = "nombre";
        }

        private void Cmbmunicipio_SelectedIndexChanged(object sender, EventArgs e)
        {
            Bindingmaestros();
        }

        private void Cmbestado_SelectedIndexChanged(object sender, EventArgs e)
        {
            string filtro;
            cmbestado.ValueMember = "codigo";
            filtro = cmbestado.SelectedValue.ToString();
            cmbmunicipio.DataSource = _municipiosManejador.ObtenerLista(filtro);
            cmbmunicipio.DisplayMember = "nombre";
        }

        private void MaestrosModal_Load(object sender, EventArgs e)
        {
            BuscarEstados();
            BuscarMunicipios("");
        }
    }
}
