﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocio.ControlEscolarApp;
using Entidades.ControlEscolarApp;
using Extensions.ControlEscolarApp;

namespace ControlEscolarApp
{
    public partial class EscuelaPresentacion : Form
    {
        private OpenFileDialog _dialogCargarLogo;
        private bool _isImagenClear = false;
        private EscuelaManejador _escuelaManejador;
        private RutasManager _rutasManager;
        private bool _isEnableBinding = false;
        private Escuela _escuela;

        public EscuelaPresentacion()
        {
            InitializeComponent();
            _dialogCargarLogo = new OpenFileDialog();
            _rutasManager = new RutasManager(Application.StartupPath);
            _escuelaManejador = new EscuelaManejador(_rutasManager);
            _escuela = new Escuela();

            _escuela = _escuelaManejador.GetEscuela();


            if (!string.IsNullOrEmpty(_escuela.IdEscuela.ToString()))
            {
                LoadEntity();
            }
            _isEnableBinding = true;
        }

        private void LoadEntity()
        {
            txtnombre.Text = _escuela.Nombre;
            txtdirector.Text = _escuela.Director;
            picLogo.ImageLocation = null;

            if (!string.IsNullOrEmpty(_escuela.Logo) && string.IsNullOrEmpty(_dialogCargarLogo.FileName))
            {
                picLogo.ImageLocation = _rutasManager.RutaLogoEscuela(_escuela);
            }
            

        }

        private void BtnAgregarLogo_Click(object sender, EventArgs e)
        {
            CargarLogo();
        }

        private void CargarLogo()
        {
            _dialogCargarLogo.Filter = "Imagen tipo(*.png)|*.png|imagen tipo (*.jpg)|*.jpg";
            _dialogCargarLogo.Title = "Cargar un archivo de imagen";
            _dialogCargarLogo.ShowDialog();

            if (_dialogCargarLogo.FileName !="")
            {
                if (_escuelaManejador.CargarLogo(_dialogCargarLogo.FileName))
                {
                    picLogo.ImageLocation = _dialogCargarLogo.FileName;
                    _isImagenClear = false;

                }
                else
                {
                    MessageBox.Show("No se pueden cargar imagenes mayores a 5 MB");
                }
            }
            {

            }
        }

        private void BrnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro los cambios se guardaran al final de la edicion","Eliminar", MessageBoxButtons.OKCancel)==DialogResult.OK)
            {
                EliminarLogo();
            }
        }

        private void EliminarLogo()
        {
            picLogo.ImageLocation = null;
            _isImagenClear = true;
        }

        private void Txtnombre_TextChanged(object sender, EventArgs e)
        {
            BindingEntity();
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            //ActivarCuadros(false);
            //ActivarBotones(true, false, false, false, false);
            if (MessageBox.Show("Estas seguro de salir sin guardar cambios", "salir", MessageBoxButtons.OKCancel) == DialogResult.OK) 
            {
                this.Close();
            }
        }

        private void Txtdirector_TextChanged(object sender, EventArgs e)
        {
            BindingEntity();
        }

        private void BindingEntity()
        {
            if (_isEnableBinding)
            {
                _escuela.Nombre = txtnombre.Text;
                _escuela.Director = txtdirector.Text;
            }
        }
        private void Save()
        {
            try
            {
                if (picLogo.Image !=null)
                {
                    if (!string.IsNullOrEmpty(_dialogCargarLogo.FileName))
                    {
                        _escuela.Logo = _escuelaManejador.GetNombreLogo(_dialogCargarLogo.FileName);
                        if (!string.IsNullOrEmpty(_escuela.Logo))
                        {
                            _escuelaManejador.GuargarLogo(_dialogCargarLogo.FileName, 1);
                            _dialogCargarLogo.Dispose();
                        }
                    }
                }
                else
                {
                    _escuela.Logo = string.Empty;

                }
                if (_isImagenClear)
                {
                    _escuelaManejador.LimpiarDocumento(1, "png");
                    _escuelaManejador.LimpiarDocumento(1, "jpg");
                }
                _escuelaManejador.Guardar(_escuela);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
    }
}
