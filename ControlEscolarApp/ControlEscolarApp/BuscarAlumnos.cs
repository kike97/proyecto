﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocio.ControlEscolarApp;
using Entidades.ControlEscolarApp;

namespace ControlEscolarApp
{
    public partial class BuscarAlumnos : Form
    {
        AlumnoManejador _alumnoManejador;
        Alumnos _alumnos;

        public BuscarAlumnos()
        {
            InitializeComponent();
            _alumnoManejador = new AlumnoManejador();
            _alumnos = new Alumnos();
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            AlumnoModal _usuariosModal = new AlumnoModal();
            _usuariosModal.ShowDialog();
            BuAlumno("");
        }

        private void BuscarAlumnos_Load(object sender, EventArgs e)
        {
            BuAlumno("");
        }
        private void BuAlumno(string filtro)
        {
            DtgAlumnos.DataSource = _alumnoManejador.ObtenerListaAlumno(filtro);
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuAlumno(TxtBuscar.Text);
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar ese registro", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    Eliminar();
                    BuAlumno("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }
        }
        private void Eliminar()
        {
            string numero = DtgAlumnos.CurrentRow.Cells["Numerodecontrol"].Value.ToString();
            _alumnoManejador.EliminarAlumno(numero);
        }

        private void BindingAlumno()
        {
            _alumnos.Ncontrol = DtgAlumnos.CurrentRow.Cells["Numerodecontrol"].Value.ToString();
            _alumnos.Nombre = DtgAlumnos.CurrentRow.Cells["Nombre"].Value.ToString();
            _alumnos.ApellidoPaterno = DtgAlumnos.CurrentRow.Cells["Apellidopaterno"].Value.ToString();
            _alumnos.ApellidoMaterno = DtgAlumnos.CurrentRow.Cells["Apellidomaterno"].Value.ToString();
            _alumnos.Sexo = DtgAlumnos.CurrentRow.Cells["Sexo"].Value.ToString();
            _alumnos.Fechanacimiento = DtgAlumnos.CurrentRow.Cells["Fechanacimiento"].Value.ToString();
            _alumnos.Correoelectronico = DtgAlumnos.CurrentRow.Cells["CorreoElectronico"].Value.ToString();
            _alumnos.Telefonocontacto = DtgAlumnos.CurrentRow.Cells["Telefonocontacto"].Value.ToString();
            _alumnos.Estado = DtgAlumnos.CurrentRow.Cells["Estado"].Value.ToString();
            _alumnos.Municipio = DtgAlumnos.CurrentRow.Cells["Municipio"].Value.ToString();
            _alumnos.Domicilio = DtgAlumnos.CurrentRow.Cells["Domicilio"].Value.ToString();
        }

        private void DtgAlumnos_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            BindingAlumno();
            AlumnoModal _alumnoModal = new AlumnoModal(_alumnos);
            _alumnoModal.ShowDialog();
            BuAlumno("");
        }
    }
}
