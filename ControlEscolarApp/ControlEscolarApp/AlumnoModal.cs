﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocio.ControlEscolarApp;
using Entidades.ControlEscolarApp;

namespace ControlEscolarApp
{
    public partial class AlumnoModal : Form
    {
        private AlumnoManejador _alumnoManejador;
        private EstadosManejador _estadosManejador;
        private MunicipiosManejador _municipiosManejador;
        private Alumnos _alumnos;
        private bool _isEnablebinding = false;
        public AlumnoModal()
        {
            InitializeComponent();
            _alumnoManejador = new AlumnoManejador();
            _estadosManejador = new EstadosManejador();
            _municipiosManejador = new MunicipiosManejador();
            _alumnos = new Alumnos();
            _isEnablebinding = true;
            BindingAlumno();
        }
        public AlumnoModal(Alumnos alumnos)
        {
            InitializeComponent();
            _alumnoManejador = new AlumnoManejador();
            _estadosManejador = new EstadosManejador();
            _municipiosManejador = new MunicipiosManejador();
            _alumnos = new Alumnos();
            _alumnos = alumnos;


            BindingAlumnoReload();

            _isEnablebinding = true;
        }
        private void BindingAlumnoReload()
        {
            TxtNoControl.Text = _alumnos.Ncontrol;
            TxtNombre.Text = _alumnos.Nombre;
            TxtApellidoPaterno.Text = _alumnos.ApellidoPaterno;
            TxtApellidoMaterno.Text = _alumnos.ApellidoMaterno;
            TxtSexo.Text = _alumnos.Sexo;
            TxtFecha.Text = _alumnos.Telefonocontacto;
            TxtCorreo.Text = _alumnos.Correoelectronico;
            TxtTelefono.Text = _alumnos.Telefonocontacto;
            CboEstado.Text = _alumnos.Estado;
            CboMunicipio.Text = _alumnos.Municipio;
            TxtDomicilio.Text = _alumnos.Domicilio;

        }
        private void BuscarEstados()
        {
            CboEstado.DataSource = _estadosManejador.ObtenerLista();
            CboEstado.DisplayMember = "nombre";
            
        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {
            BindingAlumno();
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            BindingAlumno();
            Guardar();
            this.Close();
            

            /*if (ValidarUsuario() && ValidarApellidoP() && ValidarApellidoM())
            {
                Guardar();
                this.Close();
            }*/
        }
        private void Guardar()
        {
            _alumnoManejador.GuardarAlumno(_alumnos);
        }
        private void BindingAlumno()
        {
            if (_isEnablebinding)
            {
                if (_alumnos.Ncontrol == _alumnos.Ncontrol)
                {
                    _alumnos.Ncontrol = _alumnos.Ncontrol;
                }

                _alumnos.Ncontrol = TxtNoControl.Text;
                _alumnos.Nombre = TxtNombre.Text;
                _alumnos.ApellidoPaterno = TxtApellidoPaterno.Text;
                _alumnos.ApellidoMaterno = TxtApellidoMaterno.Text;
                _alumnos.Sexo = TxtSexo.Text;
                _alumnos.Telefonocontacto = TxtFecha.Text;
                _alumnos.Correoelectronico = TxtCorreo.Text;
                _alumnos.Telefonocontacto = TxtTelefono.Text;
                _alumnos.Estado = CboEstado.Text; 
                _alumnos.Municipio = CboMunicipio.Text;
                _alumnos.Domicilio = TxtDomicilio.Text;
            }
        }

        private void TxtNoControl_TextChanged(object sender, EventArgs e)
        {
            BindingAlumno();
        }

        private void TxtNombre_TextChanged(object sender, EventArgs e)
        {
            BindingAlumno();
        }

        private void TxtApellidoPaterno_TextChanged(object sender, EventArgs e)
        {
            BindingAlumno();
        }

        private void TxtApellidoMaterno_TextChanged(object sender, EventArgs e)
        {
            BindingAlumno();
        }

        private void TxtSexo_TextChanged(object sender, EventArgs e)
        {
            BindingAlumno();
        }

        private void TxtFecha_TextChanged(object sender, EventArgs e)
        {
            BindingAlumno();
        }

        private void TxtCorreo_TextChanged(object sender, EventArgs e)
        {
            BindingAlumno();
        }

        private void CboEstado_SelectedIndexChanged(object sender, EventArgs e)
        {
            string filtro;
            CboEstado.ValueMember = "codigo";
            filtro = CboEstado.SelectedValue.ToString();
            CboMunicipio.DataSource = _municipiosManejador.ObtenerLista(filtro);
            CboMunicipio.DisplayMember = "nombre";
        }
        private void BuscarMunicipios(string filtro) 

        {
            CboEstado.ValueMember = "codigo";
            filtro = CboEstado.SelectedValue.ToString();
            CboMunicipio.DataSource = _municipiosManejador.ObtenerLista(filtro);
            CboMunicipio.DisplayMember = "nombre";
        }

        private void CboMunicipio_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindingAlumno();
        }

        private void TxtDomicilio_TextChanged(object sender, EventArgs e)
        {
            BindingAlumno();
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AlumnoModal_Load(object sender, EventArgs e)
        {
            BuscarEstados();
            BuscarMunicipios("");
        }
    }
}
