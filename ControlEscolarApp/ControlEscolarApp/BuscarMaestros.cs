﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocio.ControlEscolarApp;
using Entidades.ControlEscolarApp;

namespace ControlEscolarApp
{
    public partial class BuscarMaestros : Form
    {
        MaestrosManejador _maestrosManejador;
        Maestros _maestros;
        public BuscarMaestros()
        {
            InitializeComponent();
            _maestrosManejador = new MaestrosManejador();
            _maestros = new Maestros();
        }

        private void BindingMaestros()
        {
            _maestros.NControl = DtgMaestros.CurrentRow.Cells["Numerodecontrol"].Value.ToString();
            _maestros.Nombre = DtgMaestros.CurrentRow.Cells["Nombre"].Value.ToString();
            _maestros.Apellidop = DtgMaestros.CurrentRow.Cells["Apellidop"].Value.ToString();
            _maestros.Apellidom = DtgMaestros.CurrentRow.Cells["Apellidom"].Value.ToString();
            _maestros.FechadeNa = DtgMaestros.CurrentRow.Cells["fechadenacimiento"].Value.ToString();
            _maestros.Esta = DtgMaestros.CurrentRow.Cells["esta"].Value.ToString();
            _maestros.Muni = DtgMaestros.CurrentRow.Cells["munici"].Value.ToString();
            _maestros.Sexo = DtgMaestros.CurrentRow.Cells["sexo"].Value.ToString();
            _maestros.Correo = DtgMaestros.CurrentRow.Cells["correoelectronico"].Value.ToString();
            _maestros.Nocuenta = DtgMaestros.CurrentRow.Cells["nocuenta"].Value.ToString();
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            MaestrosModal _maestrosModal = new MaestrosModal();
            _maestrosModal.ShowDialog();
            BuMaestro("");
        }
        private void BuMaestro(string filtro)
        {
            DtgMaestros.DataSource = _maestrosManejador.ObtenerListaMaestro(filtro);
        }
        private void Eliminar()
        {
            string numero = DtgMaestros.CurrentRow.Cells["Numerodecontrol"].Value.ToString();
            _maestrosManejador.EliminarMaestroo(numero);
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro de eliminar ese registro", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    Eliminar();
                    BuMaestro("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuMaestro(TxtBuscar.Text);
        }

        private void DtgMaestros_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            BindingMaestros();
            MaestrosModal _maestroModal = new MaestrosModal(_maestros);
            _maestroModal.ShowDialog();
            BuMaestro("");
        }
    }
}
