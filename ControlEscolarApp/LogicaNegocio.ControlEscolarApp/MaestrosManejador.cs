﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolarApp;
using AccesoDatos.ContolEscolarApp;

namespace LogicaNegocio.ControlEscolarApp
{
    public class MaestrosManejador
    {
        private MaestroAccesoDatos _maestroAccesoDatos;
        public MaestrosManejador()
        {
            _maestroAccesoDatos = new MaestroAccesoDatos();
        }
        public void EliminarMaestroo(string nControl)
        {
            _maestroAccesoDatos.EliminarMaestro(nControl);
        }
        public void GuardarMaestro(Maestros maestros)
        {
            _maestroAccesoDatos.GuardarMaestro(maestros);
        }
        public List<Maestros> ObtenerListaMaestro(string filtro)
        {
            var list = new List<Maestros>();
            list = _maestroAccesoDatos.ObtenerListaMaestro(filtro);
            return list;
        }
    }
}
