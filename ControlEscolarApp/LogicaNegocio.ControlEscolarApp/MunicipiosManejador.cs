﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolarApp;
using AccesoDatos.ContolEscolarApp;
using System.Text.RegularExpressions;

namespace LogicaNegocio.ControlEscolarApp
{
   public class MunicipiosManejador
    {
        private MunicipiosAccesoDatos _municipiosAccesoDatos;
        public MunicipiosManejador()
        {
            _municipiosAccesoDatos = new MunicipiosAccesoDatos();
        }
        public List<Municipio> ObtenerLista(string filtro)
        {
            var list = new List<Municipio>();
            list = _municipiosAccesoDatos.ObtenerListaMunicipios(filtro);
            return list;
        }
    }
}
