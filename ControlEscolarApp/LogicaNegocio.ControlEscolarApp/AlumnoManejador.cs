﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolarApp;
using AccesoDatos.ContolEscolarApp;
using System.Text.RegularExpressions;


namespace LogicaNegocio.ControlEscolarApp
{
    public class AlumnoManejador
    {
        private AlumnoAccesoDatos _alumnoAccesoDatos;
        public AlumnoManejador()
        {
            _alumnoAccesoDatos = new AlumnoAccesoDatos();
        }
        public void EliminarAlumno(string nControl)
        {
            _alumnoAccesoDatos.EliminarAlumno(nControl);
        }
        public void GuardarAlumno(Alumnos alumnos)
        {
            _alumnoAccesoDatos.GuardarAlumno(alumnos);
        }
        /*private bool NombreValido(string nombre)
        {
            var regex = new Regex(@"^[A-Za-z]+( [A-Za-z]+)*$");
            var match = regex.Match(nombre);

            if (match.Success)
            {
                return true;
            }
            return false;
        }*/
        public List<Alumnos> ObtenerListaAlumno(string filtro)
        {
            var list = new List<Alumnos>();
            list = _alumnoAccesoDatos.ObtenerListaAlumno(filtro);
            return list;
        }
    }
}
