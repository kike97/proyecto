﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolarApp;
using AccesoDatos.ContolEscolarApp;
using System.Text.RegularExpressions;


namespace LogicaNegocio.ControlEscolarApp
{
    public class EstadosManejador
    {
        private EstadosAccesoDatos _estadosAccesoDatos;
        public EstadosManejador()
        {
            _estadosAccesoDatos = new EstadosAccesoDatos();
        }
        public List<Estados> ObtenerLista()
        {
            var list = new List<Estados>();
            list = _estadosAccesoDatos.ObtenerListaEstados();
            return list;
        }
    }
}
