﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using AccesoDatos.ContolEscolarApp;
using Entidades.ControlEscolarApp;
using Extensions.ControlEscolarApp;


namespace LogicaNegocio.ControlEscolarApp
{
   public class EscuelaManejador
    {
        private EscuelaAccesoDatos _escuelaAccesoDatos = new EscuelaAccesoDatos();
        private RutasManager _rutasManager;

        public EscuelaManejador(RutasManager rutasManager)
        {
            _rutasManager = rutasManager;
        }
        public void Guardar(Escuela escuela)
        {
            _escuelaAccesoDatos.Guardar(escuela);
        }
        public Escuela GetEscuela()
        {
            return _escuelaAccesoDatos.GetEscuela();
        }
        public bool CargarLogo(string fileName)
        {
            var archivoNombre = new FileInfo(fileName);

            if (archivoNombre.Length > 5000000)
            {
                return false;
            }
            return true;
        }
        public string GetNombreLogo(string fileName)
        {
            var archivoNombre = new FileInfo(fileName);
            return archivoNombre.Name;
        }
        public void LimpiarDocumento(int escuelaId, string tipoDocumento)
        {
            string rutaRepositorio = "";
            string extension = "";
            switch (tipoDocumento)
            {
                case "png":
                    rutaRepositorio = _rutasManager.RutaRepositoriosLogos;
                    extension = "*.png";
                    break;
                case "jpg":
                    rutaRepositorio = _rutasManager.RutaRepositoriosLogos;
                    extension = "*.jpg";
                    break;

            }
            string ruta = Path.Combine(rutaRepositorio, escuelaId.ToString());
            if (Directory.Exists(ruta))
            {
                var obtenerArchivos = Directory.GetFiles(ruta, extension);
                FileInfo archivoAnterior;
                if (obtenerArchivos.Length !=0)
                {
                    archivoAnterior = new FileInfo(obtenerArchivos[0]);
                    if (archivoAnterior.Exists)
                    {
                        archivoAnterior.Delete();
                    }
                }
            }
        }
        public void GuargarLogo(string fileName, int escuelaId)
        {
            if (!string.IsNullOrEmpty(fileName))
            {
                var archivoDocument = new FileInfo(fileName);
                string ruta = Path.Combine(_rutasManager.RutaRepositoriosLogos, escuelaId.ToString());
                if (Directory.Exists(ruta))
                {
                    var obtenerArchivos = Directory.GetFiles(ruta);
                    FileInfo archivoAnterior;
                    if (obtenerArchivos.Length != 0)
                    {
                        archivoAnterior = new FileInfo(obtenerArchivos[0]);

                        if (archivoAnterior.Exists)
                        {
                            archivoAnterior.Delete();
                            archivoDocument.CopyTo(Path.Combine(ruta, archivoDocument.Name));
                        }
                    }
                    else
                    {
                        archivoDocument.CopyTo(Path.Combine(ruta, archivoDocument.Name));
                    }
                }
                else
                {
                    _rutasManager.CrearRepositorioLogosEscuela(escuelaId);
                    archivoDocument.CopyTo(Path.Combine(ruta, archivoDocument.Name));
                }
            }
        }
   }
}
