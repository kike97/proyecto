﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolarApp;
using ConexionBd;
using System.Data;

namespace AccesoDatos.ContolEscolarApp
{
    public class EstadosAccesoDatos
    {
        Conexion _conexion;
        public EstadosAccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "escolar", 3306);
        }
        public List<Estados> ObtenerListaEstados()
        {
            var List = new List<Estados>();
            string consulta = string.Format("select * from estados");
            var ds = _conexion.ObtenerDatos(consulta, "estados");
            var dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var estado = new Estados
                {
                    Codigo = row["codigo"].ToString(),
                    Nombre = row["nombre"].ToString()
                };
                List.Add(estado);
            }
            return List;
        }
    }
}

