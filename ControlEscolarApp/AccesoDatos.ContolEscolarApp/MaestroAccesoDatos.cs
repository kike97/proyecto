﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolarApp;
using ConexionBd;
using System.Data;

namespace AccesoDatos.ContolEscolarApp
{
    public class MaestroAccesoDatos
    {

        Conexion _conexion;
        public MaestroAccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "escolar", 3306);
        }
        public void EliminarMaestro(string nControl)
        {
            string cadena = string.Format("delecte from maestros where Numerodecontrol = '{0}'", nControl);
            _conexion.EjecutarConsulta(cadena);
        }
        public void GuardarMaestro(Maestros maestros)
        {
            if (ObtenerNumControl(maestros.NControl) == 0)
            {
                string cadena = string.Format("insert into maestros values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}')"
                    , maestros.NControl, maestros.Nombre, maestros.Apellidop, maestros.Apellidom, maestros.FechadeNa, maestros.Esta, maestros.Muni, maestros.Sexo, maestros.Correo, maestros.Nocuenta);
                _conexion.EjecutarConsulta(cadena);
            }
            else
            {
                string cadena = string.Format("update maestros set nombre = '{0}', apellidop = '{1}', apellidom = '{2}', fechadenacimiento = '{3}', esta = '{4}', minici = '{5}', sexo = '{6}', correoelectronico = '{7}', nocuenta = '{8}' where numerodecontrol = '{9}')", maestros.Nombre, maestros.Apellidop, maestros.Apellidom, maestros.FechadeNa, maestros.Esta, maestros.Muni, maestros.Sexo, maestros.Correo, maestros.Nocuenta, maestros.NControl);
                _conexion.EjecutarConsulta(cadena);
            }
        }
        public List<Maestros> ObtenerListaMaestro(string filtro)
        {
            var list = new List<Maestros>();
            string consulta = string.Format("select * from maestros where numerodecontrol like '%{0}%'", filtro);
            var ds = _conexion.ObtenerDatos(consulta, "maestros");
            var dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var maestros = new Maestros
                {
                    NControl = row["numerodecontrol"].ToString(),
                    Nombre = row["nombre"].ToString(),
                    Apellidop = row["apellidop"].ToString(),
                    Apellidom = row["apellidom"].ToString(),
                    FechadeNa = row["fechadeNacimiento"].ToString(),
                    Esta = row["esta"].ToString(),
                    Muni = row["munici"].ToString(),
                    Sexo = row["sexo"].ToString(),
                    Correo = row["correoelectronico"].ToString(),
                    Nocuenta = row["nocuenta"].ToString()
                };
                list.Add(maestros);
            }
            return list;
        }
        public int ObtenerNumControl(string noControl)
        {
            var res = _conexion.Existencia("select count(*) from maestros where numerodecontrol='" + noControl + "'");
            return res;
        }
    }
}
