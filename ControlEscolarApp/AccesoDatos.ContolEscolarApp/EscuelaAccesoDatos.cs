﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolarApp;
using ConexionBd;
using System.Data;

namespace AccesoDatos.ContolEscolarApp
{
   public class EscuelaAccesoDatos
    {
        Conexion _conexion;
        public EscuelaAccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "escolar", 3306);
        }
        public void Guardar (Escuela escuela)
        {
            if (escuela.IdEscuela == 0)
            {
                string consulta = string.Format("INSERT INTO escuela values(null,'{0}','{1}'," + "'{2}'" + "" + "" + "" + "" + "", 
                    escuela.Nombre, escuela.Director, escuela.Logo);
                _conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("Update escuela set nombre = '{0}', director ='{1}'" + "logo = '{2}' where idescuela={3}", 
                    escuela.Nombre, escuela.Director, escuela.Logo, escuela.IdEscuela);
                _conexion.EjecutarConsulta(consulta);
            }
        }
        public Escuela GetEscuela()
        {
            var ds = new DataSet();
            string consulta = "select * from escuela";
            ds = _conexion.ObtenerDatos(consulta, "escuela");
            var dt = new DataTable();
            var escuela = new Escuela();

            foreach(DataRow row in dt.Rows)
            {
                escuela.IdEscuela = Convert.ToInt32(row["idescuela"]);
                escuela.Nombre = row["nombre"].ToString();
                escuela.Director = row["director"].ToString();
                escuela.Logo = row["logo"].ToString();

            }
            return escuela;
                    }
    }
}
