﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolarApp;
using ConexionBd;
using System.Data;

namespace AccesoDatos.ContolEscolarApp
{
    public class MunicipiosAccesoDatos
    {
        Conexion _conexion;
        public MunicipiosAccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "escolar", 3306);
        }
        public List<Municipio> ObtenerListaMunicipios(string filtro)
        {
            var List = new List<Municipio>();
            string consulta = string.Format("Select * from municipios where fk_codigo like '%{0}%'", filtro);
            var ds = _conexion.ObtenerDatos(consulta, "estados");
            var dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var munici = new Municipio
                {
                    Id = Convert.ToInt32(row["Id"]),
                    Nombre = row["Nombre"].ToString(),
                    Fkcodigo = row["Fk_codigo"].ToString()
                };
                List.Add(munici);
            }
            return List;
        }
    }
}
