﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolarApp;
using ConexionBd;
using System.Data;

namespace AccesoDatos.ContolEscolarApp
{
    public class AlumnoAccesoDatos
    {
        Conexion _conexion;
         public AlumnoAccesoDatos()
         {
             _conexion = new Conexion("localhost", "root", "", "escolar", 3306);
         }
        public void EliminarAlumno(string numeroControl)
        {
            string cadena = string.Format("delecte from alumnos where Numerodecontrol = '{0}'", numeroControl);
            _conexion.EjecutarConsulta(cadena);
        }
        public void GuardarAlumno(Alumnos alumno)
        {
            if (ObtenerNumControl(alumno.Ncontrol) == 0)
            {
                string cadena = string.Format("insert into alumnos values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}')", alumno.Ncontrol, alumno.Nombre, alumno.ApellidoPaterno, alumno.ApellidoMaterno, alumno.Sexo, alumno.Fechanacimiento, alumno.Correoelectronico, alumno.Telefonocontacto, alumno.Estado, alumno.Municipio, alumno.Domicilio);
                _conexion.EjecutarConsulta(cadena);
            }
            else
            {
                string cadena = string.Format("update alumnos set Nombre = '{0}', apellidopaterno = '{1}', apellidomaterno = '{2}', sexo = '{3}', fechadenacimiento = '{4}', correoelectronico = '{5}', telefonodecontacto = '{6}', estado = '{7}', municipio = '{8}', domicilio = '{9}' where numerodecontrol = '{10}')", alumno.Nombre, alumno.ApellidoPaterno, alumno.ApellidoMaterno, alumno.Sexo, alumno.Fechanacimiento, alumno.Correoelectronico, alumno.Telefonocontacto, alumno.Estado, alumno.Municipio, alumno.Domicilio, alumno.Ncontrol);
                _conexion.EjecutarConsulta(cadena);
            }
        }
        public List<Alumnos> ObtenerListaAlumno(string filtro)
        {
            var list = new List<Alumnos>();
            string consulta = string.Format("select * from alumnos where numerodecontrol like '%{0}%'", filtro);
            var ds = _conexion.ObtenerDatos(consulta, "alumnos");
            var dt = ds.Tables[0];

            foreach  (DataRow row in dt.Rows)
            {
                var alumno = new Alumnos
                {
                    Ncontrol = row["Numerodecontrol"].ToString(),
                    Nombre = row["Nombre"].ToString(),
                    ApellidoPaterno = row["ApellidoPaterno"].ToString(),
                    ApellidoMaterno = row["ApellidoMaterno"].ToString(),
                    Sexo = row["Sexo"].ToString(),
                    Fechanacimiento = row["FechadeNacimiento"].ToString(),
                    Correoelectronico = row["Correoelectronico"].ToString(),
                    Telefonocontacto = row["Telefonodecontacto"].ToString(),
                    Estado = row["Estado"].ToString(),
                    Municipio = row["Municipio"].ToString(),
                    Domicilio = row["Domicilio"].ToString()
                };
                list.Add(alumno);
            }
            return list;
        }
        public int ObtenerNumControl(string noControl)
        {
            var res = _conexion.Existencia("select count(*) from Alumnos where numerodecontrol='" + noControl + "'");
            return res;
        }
    }
}
