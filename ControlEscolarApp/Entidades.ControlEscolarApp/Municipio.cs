﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolarApp
{
    public class Municipio
    {
        private int _id;
        private string _nombre;
        private string _fkcodigo;

        public int Id { get => _id; set => _id = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string Fkcodigo { get => _fkcodigo; set => _fkcodigo = value; }
    }
}
