﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolarApp
{
    public class Maestros
    {
        private string _nControl;
        private string _nombre;
        private string _apellidop;
        private string _apellidom;
        private string _fechadeNa;
        private string _esta;
        private string _muni;
        private string _sexo;
        private string _correo;
        private string _nocuenta;

        public string NControl { get => _nControl; set => _nControl = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string Apellidop { get => _apellidop; set => _apellidop = value; }
        public string Apellidom { get => _apellidom; set => _apellidom = value; }
        public string FechadeNa { get => _fechadeNa; set => _fechadeNa = value; }
        public string Esta { get => _esta; set => _esta = value; }
        public string Muni { get => _muni; set => _muni = value; }
        public string Sexo { get => _sexo; set => _sexo = value; }
        public string Correo { get => _correo; set => _correo = value; }
        public string Nocuenta { get => _nocuenta; set => _nocuenta = value; }
    }
}
