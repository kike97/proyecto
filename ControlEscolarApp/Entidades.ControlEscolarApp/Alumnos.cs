﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolarApp
{
    public class Alumnos
    {
        private string _ncontrol;
        private string _nombre;
        private string _apellidoMaterno;
        private string _apellidoPaterno;
        private string _sexo;
        private string _fechanacimiento;
        private string _correoelectronico;
        private string _telefonocontacto;
        private string _estado;
        private string _municipio;
        private string _domicilio;

        public string Ncontrol { get => _ncontrol; set => _ncontrol = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string ApellidoMaterno { get => _apellidoMaterno; set => _apellidoMaterno = value; }
        public string ApellidoPaterno { get => _apellidoPaterno; set => _apellidoPaterno = value; }
        public string Sexo { get => _sexo; set => _sexo = value; }
        public string Fechanacimiento { get => _fechanacimiento; set => _fechanacimiento = value; }
        public string Correoelectronico { get => _correoelectronico; set => _correoelectronico = value; }
        public string Telefonocontacto { get => _telefonocontacto; set => _telefonocontacto = value; }
        public string Estado { get => _estado; set => _estado = value; }
        public string Municipio { get => _municipio; set => _municipio = value; }
        public string Domicilio { get => _domicilio; set => _domicilio = value; }
    }
}
